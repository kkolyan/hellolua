package com.nplekhanov.exp;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;
import org.apache.commons.io.IOUtils;
import org.luaj.vm2.Globals;
import org.luaj.vm2.LuaError;
import org.luaj.vm2.LuaFunction;
import org.luaj.vm2.LuaNil;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.Varargs;
import org.luaj.vm2.lib.VarArgFunction;
import org.luaj.vm2.lib.jse.JsePlatform;

public class LuaJ {
    public static void runScript(String scriptLocation) throws IOException {
        try {
            Globals globals = JsePlatform.standardGlobals();
            globals.get("io").set("read", new VarArgFunction() {
                Scanner scanner = new Scanner(System.in);
                @Override
                public Varargs invoke(Varargs args) {
                    return LuaValue.valueOf(scanner.nextLine());
                }
            });
            globals.STDIN = System.in;
            String scriptText = IOUtils.resourceToString(scriptLocation, StandardCharsets.UTF_8, Thread.currentThread().getContextClassLoader());
            LuaValue chunk = globals.load(scriptText, scriptLocation);
            chunk.call();
        } catch (LuaError e) {
            System.err.println(e.getMessage());
        }
    }
}
