package com.nplekhanov.exp;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import org.apache.commons.io.IOUtils;
import org.python.util.PythonInterpreter;

public class JythonSeaCombat {
    public static void main(String[] args) throws IOException {
        PythonInterpreter interpreter = new PythonInterpreter();
        interpreter.exec(IOUtils.resourceToString("seacombat.py", StandardCharsets.UTF_8, Thread.currentThread().getContextClassLoader()));
    }
}
