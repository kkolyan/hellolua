field = {}
ship_ids = "0123456789ABCDEFGHIJKLMOOQRSTUVWXYZ"
col_names = "ABCDEFGHIJ"
row_count = 10
ships_template = { 4, 3, 2, 1 }
ships = {}
holes = {}
aliveShips = 0
random = true
padRowNumber = 3

showEnemies = false
logEnabled = false

shootsSpent = 0

log = {
    write = function(v)
        if logEnabled then
            io.write(v)
        end
    end,
    writeln = function(v)
        if logEnabled then
            print(v)
        end
    end
}

function cellLabel(key)
    local value = field[key]
    if not value then
        if holes[key] then
            return "."
        end
        return " "
    end
    local ship = ships[value]
    if ship.health == 0 then
        return "x"
    end
    if not ship.cells[key] then
        return "o"
    end
    if showEnemies then
        return ship_ids:sub(value, value)
    end
    return " "
end

function printField()
    io.write(rpad("", padRowNumber))
    io.write("| ")
    for col = 1, #col_names do
        io.write(col_names:sub(col, col))
        io.write(" | ")
    end
    print()
    for row = 1, row_count do
        io.write(rpad(row, padRowNumber))
        if row % 2 == 0 then
            io.write("| ")
        else
            io.write("  ")
        end
        for col = 1, #col_names do
            local key = col_names:sub(col, col) .. row
            io.write(cellLabel(key))
            if row % 2 == 0 then
                io.write(" | ")
            else
                io.write("   ")
            end
        end
        print()
    end
end

function rpad(v, length)
    local s = tostring(v)
    while #s < length do
        s = s .. " "
    end
    return s
end

function generateShips()
    for length, count in pairs(ships_template) do
        for _ = 1, count do
            generateShip(length)
        end
    end
end

function createKey(x, y, row, col, vertical)
    local ax
    local ay
    if (vertical) then
        ax = col + x
        ay = row + y
    else
        ax = col + y
        ay = row + x
    end
    if ax < 1 or ax > #col_names or ay < 1 or ay > row_count then
        return nil
    end
    return col_names:sub(ax, ax) .. ay
end

function createCellsIfFree(row, col, vertical, length)
    local cells = {}
    for y = 0, length + 1 do
        for x = -1, 1 do
            log.write("  x: " .. x .. ", y: " .. y .. ", length: " .. length .. ", ")
            if x == 0 or (y > 0 and y < length + 1) then
                local nonBorder = x == 0 and y > 0 and y < length + 1
                local key = createKey(x, y, row, col, vertical)
                if not key then
                    if nonBorder then
                        log.writeln("cell out")
                        return nil
                    else
                        log.writeln("border out")
                    end
                else
                    if field[key] then
                        log.writeln(key .. ": fail")
                        return nil
                    end
                    if nonBorder then
                        table.insert(cells, key)
                        log.writeln(key .. ": cell")
                    else
                        log.writeln(key .. ": border")
                    end
                end
            else
                log.writeln("skip")
            end
        end
    end

    return cells
end

function generateShip(length)
    local attempt = 0;

    local ship = {
        shipId = shipId,
        cells = {},
        health = length
    }
    table.insert(ships, ship)
    ship.shipId = #ships

    aliveShips = aliveShips + 1

    log.writeln("shipId: " .. ship_ids:sub(ship.shipId, ship.shipId))

    while true do
        attempt = attempt + 1
        if attempt > 1000 then
            error("failed to fill field: dead loop")
        end

        local row = math.random(row_count)
        local col = math.random(#col_names)
        local vertical = math.random(2) == 1

        log.writeln("  attempt: " .. attempt .. ", row: " .. row .. ", col: " .. col .. ", vertical: " .. tostring(vertical))

        local cells = createCellsIfFree(row, col, vertical, length)
        if cells then

            for i, key in ipairs(cells) do
                field[key] = ship.shipId
                ship.cells[key] = true
            end

            log.writeln("ship " .. length .. " " .. ship_ids:sub(ship.shipId, ship.shipId))
            break
        end
    end
end

function shoot(cellKey)
    shootsSpent = shootsSpent + 1
    local shipId = field[cellKey]
    local ship
    if shipId then
        ship = ships[shipId]
    end
    if ship then
        if ship.cells[cellKey] then
            ship.cells[cellKey] = false
            ship.health = ship.health - 1
            if ship.health > 0 then
                print("Hit!")
            else
                aliveShips = aliveShips - 1
                if aliveShips > 0 then
                    print("Hit and killed!")
                else
                    print("Hit, killed and exterminated in " .. shootsSpent .. "!")
                end
            end
        end
    else
        holes[cellKey] = true
        print("Miss...")
    end
end

function mainLoop()
    print("generating field...")
    generateShips()
    printField()
    while aliveShips > 0 do
        io.write("(" .. shootsSpent .. ") Choose target: ")
        io.flush()
        local line = io.read()
        print()
        if line then
            if line == "q" then
                return
            end
            shoot(string.upper(line))
            printField()
        else
            print("Cannot read input")
            return
        end
    end
end

if random then
    math.randomseed(os.time())
end
mainLoop()