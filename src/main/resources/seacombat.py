import sys
import random

field = {}
ship_ids = "0123456789ABCDEFGHIJKLMOOQRSTUVWXYZ"
col_names = "ABCDEFGHIJ"
row_count = 10
ships_template = {1: 4, 2: 3, 3: 2, 4: 1}
ships = {}
holes = {}
aliveShips = 0
is_random = True
padRowNumber = 3

showEnemies = False
logEnabled = False

shootsSpent = 0


class Log:
    def write(self, v):
        if logEnabled:
            sys.stdout.write(v)

    def writeln(self, v):
        if logEnabled:
            print(v)


log = Log()


def cellLabel(key):
    value = field.get(key)
    if not value:
        if holes.get(key):
            return "."

        return " "

    ship = ships[value]
    if ship.health == 0:
        return "x"

    if not ship.cells.get(key):
        return "o"

    if showEnemies:
        return ship_ids[value]

    return " "


def printField():
    sys.stdout.write(rpad("", padRowNumber))
    sys.stdout.write("| ")
    for col in range(0, len(col_names)):
        sys.stdout.write(col_names[col])
        sys.stdout.write(" | ")

    print("")
    for row in range(1, row_count + 1):
        sys.stdout.write(rpad(row, padRowNumber))
        if row % 2 == 0:
            sys.stdout.write("| ")
        else:
            sys.stdout.write("  ")

        for col in range(0, len(col_names)):
            key = col_names[col] + str(row)
            sys.stdout.write(cellLabel(key))
            if row % 2 == 0:
                sys.stdout.write(" | ")
            else:
                sys.stdout.write("   ")

        print("")


def rpad(v, length):
    s = str(v)
    while len(s) < length:
        s = s + " "

    return s


def generateShips():
    for length, count in ships_template.iteritems():
        for _ in range(0, count):
            generateShip(length)


def createKey(x, y, row, col, vertical):
    if (vertical):
        ax = col + x
        ay = row + y
    else:
        ax = col + y
        ay = row + x

    if ax < 0 or ax >= len(col_names) or ay < 1 or ay > row_count:
        return None

    return col_names[ax] + str(ay)


def createCellsIfFree(row, col, vertical, length):
    cells = []
    for y in range(-1, length + 1):
        for x in range(-1, 2):
            log.write("  x: %s, y: %s, length: %s, " % (x, y, length))
            if x == 0 or (y >= 0 and y < length):
                nonBorder = x == 0 and y >= 0 and y < length
                key = createKey(x, y, row, col, vertical)
                if not key:
                    if nonBorder:
                        log.writeln("cell out")
                        return None
                    else:
                        log.writeln("border out")

                else:
                    if field.get(key):
                        log.writeln("%s: fail" % key)
                        return None

                    if nonBorder:
                        cells.append(key)
                        log.writeln("%s: cell" % key)
                    else:
                        log.writeln("%s: border" % key)


            else:
                log.writeln("skip")

    return cells


class Ship:
    def __init__(self):
        self.cells = {}
        self.health = 0
        self.shipId = None

    def __repr__(self):
        return str(self.__dict__)


def generateShip(length):
    attempt = 0

    ship = Ship()
    ship.health = length
    ship.shipId = len(ships) + 1
    ships[ship.shipId] = ship

    global aliveShips
    aliveShips = aliveShips + 1

    log.writeln("shipId: " + ship_ids[ship.shipId])

    while True:
        attempt = attempt + 1
        if attempt > 1000:
            raise Exception("failed to fill field: dead loop")

        row = random.randint(1, row_count)
        col = random.randint(0, len(col_names) - 1)
        vertical = random.randint(0, 1) == 0

        log.writeln("  attempt: %s, row: %s, col: %s, vertical: %s" % (attempt, row, col, vertical))

        cells = createCellsIfFree(row, col, vertical, length)
        if cells:

            for key in cells:
                field[key] = ship.shipId
                ship.cells[key] = True

            log.writeln("ship %s %s" % (length, ship_ids[ship.shipId]))
            break


def shoot(cellKey):
    global shootsSpent
    shootsSpent = shootsSpent + 1
    shipId = field.get(cellKey)

    ship = None
    if shipId:
        ship = ships.get(shipId)

    if ship:
        if ship.cells.get(cellKey):
            ship.cells[cellKey] = False
            ship.health = ship.health - 1
            if ship.health > 0:
                print("Hit!")
            else:
                global aliveShips
                aliveShips = aliveShips - 1
                if aliveShips > 0:
                    print("Hit and killed!")
                else:
                    print("Hit, killed and exterminated in %s!" % shootsSpent,)

    else:
        holes[cellKey] = True
        print("Miss")


def mainLoop():
    print("generating field")
    generateShips()
    printField()
    while aliveShips > 0:
        log.writeln(field)
        log.writeln(ships)
        log.writeln(holes)
        sys.stdout.write("(%s) Choose target: " % shootsSpent, )
        sys.stdout.flush()
        line = sys.stdin.readline()
        print()
        if line:
            if line.strip() == "q":
                return

            shoot(line.strip().upper())
            printField()
        else:
            print("Cannot read input")
            return


if is_random:
    random.seed()
else:
    random.seed(1)

mainLoop()
